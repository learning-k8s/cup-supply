FROM alpine:latest

RUN apk update

run apk --no-cache add openjdk8-jre

RUN adduser -D appuser
WORKDIR /home/appuser
USER appuser

ADD ./build/libs/cup-supply-0.4.jar app.jar

CMD java -jar ${ADDITIONAL_OPTS} app.jar

EXPOSE 9001
